import React from 'react';

const NumOfTries = props => {
    return (
        <span>Tries: {props.num.toString()}</span>
    )
}

export default NumOfTries;