import React, {Component} from 'react';
import './App.css';
import Cell from "./Cell/Cell";
import './Cell/Cell.css';
import ResetButton from "./ResetButton";
import NumOfTries from "./NumOfTries";

class App extends Component {
    state = {
        cells: [],
        randomNum: '',
        numOfTries: 0
    };

    createGameSquare = () => {
        const cells = [];
        const randomNum = Math.floor(Math.random() * 10 + 1);
        const numOfTries = 0;
        let cell = "";
        for (let i = 0; i < 36; i++) {
            const hasitem = i === randomNum ? 'o' : '';
            cell = {id: i, hasitem, isClicked: false};
            cells.push(cell);
        }

        this.setState({cells, randomNum, numOfTries});
    };

    hideCell = (id) => {
        const cells = [...this.state.cells];
        let numOfTries = this.state.numOfTries;
        numOfTries++;
        cells[id].isClicked = true;
        this.setState({cells, numOfTries});
    }


    render() {
        return (
            <div className="App">
                <ResetButton clicked={this.createGameSquare}/>
                <div className="gameSquareFrame">
                    <div className="gameSquare">
                        {this.state.cells.map(cell => {
                            return (
                            <Cell
                                key={cell.id}
                                id={cell.id}
                                hasitem={cell.hasitem}
                                isClicked={cell.isClicked}
                                hideCell={() => this.hideCell(cell.id)}
                            />)
                        })}
                    </div>
                </div>
                <NumOfTries num={this.state.numOfTries}/>
            </div>
        );
    }
}

export default App;
