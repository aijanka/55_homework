import React from 'react';

const ResetButton = props => {
    return (
        <button onClick={props.clicked}>Reset</button>
    )
}

export default ResetButton;