import React from 'react';

const Cell = (props) => {
    const classNames = ['gameCell'];
    const addclass = () => {
        classNames.push('clickedCell');
    };
    let showItem = '';

    if(props.isClicked){
        addclass();
        showItem = props.hasitem;
    }
    return (
        <button
            id={props.id}
            hasitem={props.hasitem}
            onClick={props.hideCell}
            disabled={props.isClicked}
            className={classNames.join(' ')}
        >
            {showItem}
        </button>
    )
};

export default Cell;
